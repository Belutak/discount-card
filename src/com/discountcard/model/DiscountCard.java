package com.discountcard.model;

import java.util.ArrayList;
import java.util.List;

public abstract class DiscountCard {

    private List<Customer> customerList;
    private double monthlyTurnover;
    protected double discountRate;

    public abstract double calculateDiscount(double monthlyTurnover, double valueOfPurchase);

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public void addCustomer(Customer customer) {
        if (customerList == null) {
            customerList = new ArrayList<>();
        }
        customerList.add(customer);
    }

}
