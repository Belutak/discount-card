package com.discountcard.model.discountcardimpl;

import com.discountcard.model.DiscountCard;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SilverDiscountCard extends DiscountCard {

    public SilverDiscountCard() {
        setDiscountRate(2.0);
    }

    @Override
    public double calculateDiscount(double monthlyTurnover, double valueOfPurchase) {

        if(monthlyTurnover > 300)
            discountRate = 3.5;

        return new BigDecimal(valueOfPurchase * discountRate / 100).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

}
