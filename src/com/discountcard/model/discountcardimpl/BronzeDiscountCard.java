package com.discountcard.model.discountcardimpl;

import com.discountcard.model.DiscountCard;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BronzeDiscountCard extends DiscountCard {

    public BronzeDiscountCard() {
        setDiscountRate(0.0);
    }

    @Override
    public double calculateDiscount(double monthlyTurnover, double valueOfPurchase) {
        //we change value of discountRate depending of turnover
        if (monthlyTurnover >= 100 && monthlyTurnover <= 300) {
            discountRate = 1.0;
        } else if (monthlyTurnover > 300) {
            discountRate = 2.5;
        }
        //we cast value of a discount in BigDecimal so we can round it to 2 digits
        return new BigDecimal(valueOfPurchase * discountRate / 100).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
