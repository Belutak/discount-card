package com.discountcard.model.discountcardimpl;

import com.discountcard.model.DiscountCard;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class GoldDiscountCard extends DiscountCard {


    public GoldDiscountCard() {
        setDiscountRate(2.0);
    }

    @Override
    public double calculateDiscount(double monthlyTurnover, double valueOfPurchase) {

        while (monthlyTurnover >= 100) {
            if (discountRate <= 9) {
                discountRate++;
            }
            monthlyTurnover -= 100;
        }

        return new BigDecimal(valueOfPurchase * discountRate / 100).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
