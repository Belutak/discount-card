package com.discountcard.model;

public abstract class Customer {

    private String customerInfo;

    public Customer(String customerInfo) {
        this.customerInfo = customerInfo;
    }

    public String getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(String customerInfo) {
        this.customerInfo = customerInfo;
    }
}
