package com.discountcard.create;

import com.discountcard.model.DiscountCard;

public abstract class PayDesk {

    public void printBillReceipt(String cardType, double monthlyTurnover, double valueOfPurchase) {

        DiscountCard discountCard;
        discountCard = createDiscountCard(cardType, monthlyTurnover, valueOfPurchase);
        /*
        calling calculateDiscount() method so we have discountRate calculated, since it would print pre-calculated value
        because in required order of printing output we call calculateDiscount() method after we print discountRate
        */
        double discount = discountCard.calculateDiscount(monthlyTurnover, valueOfPurchase);

        System.out.println(cardType.substring(0, 1).toUpperCase() + cardType.substring(1) + " Discount Card: ");
        System.out.println("Purchase value: $" + valueOfPurchase);
        System.out.println("Discount rate: " + discountCard.getDiscountRate() + "%");
        System.out.println("Discount: $" + discount);
        System.out.println("Total: $" + (valueOfPurchase - discount) + "\n");
    }

    protected abstract DiscountCard createDiscountCard(String cardType, double monthlyTurnover, double purchaseValue);
}
