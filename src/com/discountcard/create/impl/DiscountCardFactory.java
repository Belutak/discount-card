package com.discountcard.create.impl;

import com.discountcard.create.PayDesk;
import com.discountcard.model.DiscountCard;
import com.discountcard.model.discountcardimpl.BronzeDiscountCard;
import com.discountcard.model.discountcardimpl.GoldDiscountCard;
import com.discountcard.model.discountcardimpl.SilverDiscountCard;

public class DiscountCardFactory extends PayDesk {

    @Override
    protected DiscountCard createDiscountCard(String cardType, double monthlyTurnover, double valueOfPurchase) {
        //we must instantiate dicountCard first because it could pass through all if statements and remain non instantiated
        DiscountCard discountCard = null;
        //compare passed value with adequate string and create adequate instance of DiscountCard
        if (cardType.equals("bronze")) {
            discountCard = new BronzeDiscountCard();

        } else if (cardType.equals("silver")) {
            discountCard = new SilverDiscountCard();

        } else if (cardType.equals("gold")) {
            discountCard = new GoldDiscountCard();
        }

        return discountCard;
    }
}
