package com.discountcard;

import com.discountcard.create.PayDesk;
import com.discountcard.create.impl.DiscountCardFactory;

public class Main {

    public static void main(String[] args) {
        //make DiscountCardFactory() of PayDesk type, which will decide(based on passed string) which calculateDiscount method to use
        PayDesk payDesk = new DiscountCardFactory();

        //payDesk class calls its method and passes mock values, string for type of card who's method we are going use to calculate discount
        payDesk.printBillReceipt("bronze", 301, 103);

        payDesk.printBillReceipt("silver", 301, 113);

        payDesk.printBillReceipt("gold", 799, 134);
    }
}
